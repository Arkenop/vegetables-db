[//]: # (---)

[//]: # (tags:)

[//]: # (- ⚠ Need review ⚠)

[//]: # (---)

[//]: # (Thank you very much for your contribution to the open source world!)



[//]: # (1. Replace <vegetable-name> by the vegetables name in lowercase)
[//]: # (2. download the vegetable icon in the docs/assets/icons/ directory)
[//]: # (The image will not be visible during the development process)
<figure markdown>
  ![Image title](https://gitlab.com/m9712/vegetables-db/-/raw/main/docs/assets/icons/tomato.png){ width="150" }
</figure>

[//]: # (3. Replace <Author name> and <Author-website>)
[//]: # (4. Replace <image-url> by the link where you found the image)
- [Author: Freepik](https://www.freepik.com/)
- [image info](https://www.flaticon.com/fr/icone-gratuite/tranche-de-tomate_907530)

### JSON

[//]: # (Please, replace <*> by the correct information)
[jsonStart]:  # (Please don't remove this line)
```json
{
  "id": "toma",
  "name": "Tomato",
  "type": "fruit",
  "fromTree": false,
  "family": "Solanaceae",
  "synopsis": "Very simple to consume, it lends itself to an infinite number of preparations. Very rich in nutrition, it has real advantages for your well-being. All its qualities make it the most consumed vegetable in France.",
  "preservation": "3 to 4 days at room temperature",
  "nutritional": "20 kcal for 100g",
  "vitamin": "Rich in fiber and vitamin C",
  "history": "Discovered in South America in the 16th century, the tomato takes its name from an Aztec word: \"Tomatl\". After crossing the ocean, it reached the south of Europe. The Italians called it \"golden apple\" and the Provençals \"love apple\". Considered poisonous, its plants were cultivated for their ornamental qualities. The agronomist Olivier de Serres recommended it for the decoration of arbors. Its properties as a vegetable fruit were only discovered in the middle of the 18th century. It then gained all the tables of southern Europe. In France, the Provençals were the first to consume it. It was prepared in the form of sauces more or less raised.",
  "imagePath": "../../../assets/vegetables/icons/tomato.png",
  "cultureSheets": {
    "monthOfPlanting": 5,
    "monthOfHarvest": 7,
    "plantingInterval": 3,
    "plantation": "In the second part of May, as soon as there is no risk of frost. Choose well-built plants.",
    "harvest": "From July to September depending on the variety. To pick ripe tomatoes, grasp each one with your hands and pull it out, turning it like a pear. Cut off the stems of underripe tomatoes that you want to keep for a while. Before winter, wrap the last harvested fruits in newspaper, they will finish ripening.",
    "soil": "Lightly loose and rich in humus.",
    "tasks": "Keep a single stem and pinch off the shoots (false buds) that appear in the leaf axils. Tie the stem to a stake as the plant grows by securing it with raffia. Water regularly, preferably in the morning, avoiding wetting the foliage to avoid the development of mildew. Plant a row of marigolds nearby. Their yellow color and smell are repulsive. Keep the soil mulched and loose. Weed regularly. Be careful not to remove leaves in the belief that this will hasten fruit ripening, but at the end of the season you can remove yellowed or diseased leaves to allow the last of the fruit to ripen. To prevent fungal diseases, spray with Bordeaux mixture at planting,after the first pruning, and if the weather is too wet, a third time, avoiding the already formed fruits. As soon as the plants have recovered, you can also bring 1.5 liter of liquid manure for a 10 liter watering can and water the tomato plants twice at 15 days interval. Caution: Nettle manure is rich in nitrogen, do not overuse it at the risk of seeing the leaves develop to the detriment of the fruits.",
    "favourableAssociation": ["Celery", "Bean", "Onion", "Parsley", "Leek", "Basil", "Chives", "Nasturtium", "Lavender"],
    "unfavourableAssociation": null,
    "favourablePrecedents": null,
    "unfavourablePrecedents": null
  }
}
```
[jsonEnd]: # (Please don't remove this line)
### Sources

[//]: # (Please list all sources used)
- [lesfruitsetlegumesfrais.fr](https://www.lesfruitsetlegumesfrais.com/fruits-legumes/legumes-fruits/tomate) - FR
- Book: "Potager en carrés" - Marabout 2011 page: 123

### Contributors

[//]: # (If you modify the file you can put your name at the top of the list)

* **Adrien DERACHE** - 2022 - a.d44@tuta.io
