[//]: # (---)
[//]: # (tags:)
[//]: # (- ⚠ Need review ⚠)
[//]: # (---)

[//]: # (Thank you very much for your contribution to the open source world!)

[//]: # (If the JSON is not complete or if there are problems, uncomment the 4 lines at the top.)

[//]: # (To add a vegetable please follow the next steps)
[//]: # (Everything must be written in English, it will be translated later !)

[//]: # (1. Replace <vegetable-name> by the vegetables name in lowercase)
[//]: # (2. download the vegetable icon in the docs/assets/icons/ directory)
[//]: # (The image will not be visible during the development process)
<figure markdown>
  ![Image title](https://gitlab.com/m9712/vegetables-db/-/raw/main/docs/assets/icons/spinach.png){ width="150" }
</figure>

[//]: # (3. Replace <Author name> and <Author-website>)
[//]: # (4. Replace <image-url> by the link where you found the image)
- [Author: Freepik](https://www.freepik.com/)
- [image info](https://www.flaticon.com/fr/icone-premium/epinard_2769830?term=epinard&page=1&position=18&page=1&position=18&related_id=2769830&origin=search)

### JSON

[//]: # (https://m9712.gitlab.io/vegetables-db/Getting%20started/add-vegetables/ for more information)
[//]: # (Please, replace <*> by the correct information)
[//]: # (The "id" is the first four letters of the name in lower case)
[jsonStart]:  # (Please don't remove this line)
```json
{
  "id": "spin",
  "name": "Spinach",
  "type": "vegetable",
  "family": "Chenopodiaceae",
  "synopsis": "Known for its nutritional qualities and energy benefits, it is used in the composition of tasty dishes. A food for well-being, it is worth discovering or rediscovering for its flavor!",
  "preservation": "No more than 2 days in the refrigerator",
  "nutritional": "28 kcal for 100g",
  "vitamin": "Rich in fiber and vitamin K1",
  "history": "Very present in the Arab cultures, spinach was used in poultices in the treatment of liver and stomach pains. It seems that Greeks and Romans did not know these green leaves in Antiquity.",
  "imagePath": "../../../assets/vegetables/icons/spinach.png",
  "cultureSheets": {
    "monthOfPlanting": [3, 5],
    "monthOfHarvest": [4, 6],
    "plantingInterval": 4,
    "plantation": "Spinach is greedy for compost and requires regular watering. By playing on the variety (hundreds are adapted to spring, others to fall), it can be consumed all year round, except in climates with hot and dry summers. ",
    "harvest": "35 to 45 days after sowing",
    "soil": "Neutral or calcareous, rich and well drained",
    "tasks": "Plant spinach in part shade, in well-packed soil. Hoe, weed and water without excess.",
    "favourableAssociation": ["Cabbage", "Radish", "Tomato", "Bean"],
    "unfavourableAssociation": ["Beetroot"],
    "favourablePrecedents": ["Bean", "Carrot"],
    "unfavourablePrecedents": null
  }
}
```
[jsonEnd]: # (Please don't remove this line)

### Sources

[//]: # (Please list all sources used)
- [lesfruitsetlegumesfrais.fr](https://www.lesfruitsetlegumesfrais.com/fruits-legumes/legumes-feuilles/epinard) - FR
- ["Ma bible de la permaculture"](https://www.editionsleduc.com/produit/2087/9791028516901/ma-bible-de-la-permaculture) by BLAISE LECLERC page: 229 - FR
- Book: "Potager en carrés" - Marabout 2011 page: 117 and 59

### Contributors

[//]: # (Replace <contributor-name> by your name)
[//]: # (If you modify the file you can put your name at the top of the list)

* **DERACHE Adrien** - 2022 - a.d44@tuta.io
