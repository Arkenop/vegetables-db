[//]: # (---)
[//]: # (tags:)
[//]: # (- ⚠ Need review ⚠)
[//]: # (---)

[//]: # (Thank you very much for your contribution to the open source world!)

[//]: # (If the JSON is not complete or if there are problems, uncomment the 4 lines at the top.)

[//]: # (To add a vegetable please follow the next steps)
[//]: # (Everything must be written in English, it will be translated later !)

[//]: # (1. Replace <vegetable-name> by the vegetables name in lowercase)
[//]: # (2. download the vegetable icon in the docs/assets/icons/ directory)
[//]: # (The image will not be visible during the development process)
<figure markdown>
  ![Image title](https://gitlab.com/m9712/vegetables-db/-/raw/main/docs/assets/icons/pepper.png){ width="150" }
</figure>

[//]: # (3. Replace <Author name> and <Author-website>)
[//]: # (4. Replace <image-url> by the link where you found the image)
- [Author: Freepik](https://www.freepik.com/)
- [image info](https://www.flaticon.com/fr/icone-gratuite/poivrons_6969365?term=poivron&page=1&position=26&page=1&position=26&related_id=6969365&origin=search)

### JSON

[//]: # (https://m9712.gitlab.io/vegetables-db/Getting%20started/add-vegetables/ for more information)
[//]: # (Please, replace <*> by the correct information)
[//]: # (The "id" is the first four letters of the name in lower case)
[jsonStart]:  # (Please don't remove this line)
```json
{
  "id": "pepp",
  "name": "Pepper",
  "type": "vegetable",
  "family": "Solanaceae",
  "synopsis": "A fruit-vegetable like the tomato, it can be easily integrated into all sunny recipes. Low in calories, it is also appreciated for its vitamins.",
  "preservation": "8 days in the crisper",
  "nutritional": "35 kcal for 100g",
  "vitamin": "Rich in vitamin C",
  "history": "Certainly native to South America, the bell pepper was probably first cultivated in Mexico. Seeds dating back 5,000 years have been found there during archaeological excavations.",
  "imagePath": "../../../assets/vegetables/icons/pepper.png",
  "cultureSheets": {
    "monthOfPlanting": [5, 6],
    "monthOfHarvest": [8, 11],
    "plantingInterval": null,
    "plantation": "Their cultivation is similar to that of eggplant: they require a lot of heat and sowing should be done under cover from February for planting in May.",
    "harvest": "end of July - November. Harvest the fruits by cutting them with secateurs behind the stalk",
    "soil": "Light, cool and loose",
    "tasks": "Hoe, weed and water frequently to keep the soil fresh.",
    "favourableAssociation": ["Carrot", "Tomato", "Cabbage"],
    "unfavourableAssociation": null,
    "favourablePrecedents": null,
    "unfavourablePrecedents": null
  }
}
```
[jsonEnd]: # (Please don't remove this line)

### Sources

[//]: # (Please list all sources used)
- [lesfruitsetlegumesfrais.fr](https://www.lesfruitsetlegumesfrais.com/fruits-legumes/legumes-fruits/poivron) - FR
- ["Ma bible de la permaculture"](https://www.editionsleduc.com/produit/2087/9791028516901/ma-bible-de-la-permaculture) by BLAISE LECLERC page: 264 - FR
- Book: "Potager en carrés" - Marabout 2011 page: 121

### Contributors

[//]: # (Replace <contributor-name> by your name)
[//]: # (If you modify the file you can put your name at the top of the list)

* **DERACHE Adrien** - 2022 - a.d44@tuta.io
