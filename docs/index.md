# Welcome in MyGarden


MyGarden is an open source app to handle your organic garden !
It is open source under GPLv3 and is maintained by a community of developers that work for free on their free time.

## Getting Started :truck:

Start a new project by following our quick [Getting Started](./Getting started/Development_Setup/) guide. 
We would love to hear from you! If you have any feedback or run into issues using our app, please file an issue on this [repository](https://gitlab.com/m9712/mygarden).

## Get in touch :speech_balloon:
* [:material-discord: Discord](https://discord.gg/CTVrrCa2YQ)
* [:material-matrix: Element](https://matrix.to/#/#MyGarden:matrix.org)

## Join the team :family:
There are many ways to contribute, of which development is only one!
