#Add Vegetables Fruits and Plants
Thank for your contribution to MyGarden ! Here the steps to add vegetables:

* Visit [MyGardenDB](https://gitlab.com/m9712/vegetables-db) and clone or fork the project on your computer or use the Web IDE.
* All you need is to create a new markdown file into "docs/MyGardenDB" and past the following [template](https://gitlab.com/m9712/vegetables-db/-/raw/main/.gitlab/templates/new-vegetables.md).

!!! info

    The use of an IDE with syntaxic color is recommended for more visibility.
    The following are a list of editors and IDEs that we think will give you the most effective support.

    - [Visual Studio Code](https://code.visualstudio.com/) - Free
    - [Atom](https://atom.io/) - Free
    - [WebStorm](https://www.jetbrains.com/webstorm/) - Not free

!!! tip

    Install mkdocs-material with `pip install mkdocs-material` and run: `mkdocs serve` to see you work on your browser.

You need get the following information:

| Properties                | Description                                                                                         | Type                                                                                                                       |
|---------------------------|-----------------------------------------------------------------------------------------------------|----------------------------------------------------------------------------------------------------------------------------|
| `id`                      | Unique identifier with the four three letters of the name in lower case                             | <span class="text-blue">string</span>                                                                                      |
| `name`                    | The vegetable / fruit / plant name                                                                  | <span class="text-blue">string</span>                                                                                      |
| `type`                    | Is It a plant a fruit or a vegetable... ?                                                           | <span class="text-blue">'fruit'</span>, <span class="text-blue">'vegetable'</span>, <span class="text-blue">'plant'</span> |
| `fromTree`                | Does it come from a tree? if yes then the rotation is disabled                                      | <span class="text-blue">'true'</span>,  <span class="text-blue">'false'</span>                                             |
| `family`                  | The family                                                                                          | <span class="text-blue">string</span>                                                                                      |
| `synopsis`                | A short summary to present the vegetable...                                                         | <span class="text-blue">string</span>                                                                                      |
| `preservation`            | The best way to preserve it                                                                         | <span class="text-blue">string</span>, <span class="text-blue">null</span>                                                 |
| `nutritional`             | Energy intake in kcal per 100g                                                                      | <span class="text-blue">string</span>, <span class="text-blue">null</span>                                                 |
| `vitamin`                 | The most important vitamin intake                                                                   | <span class="text-blue">string</span>, <span class="text-blue">null</span>                                                 |
| `history`                 | The history of the vegetable in a few lines                                                         | <span class="text-blue">string</span>                                                                                      |
| `imagePath`               | A great icon from [Flaticon](https://www.flaticon.com/) (recommended)                               | <span class="text-blue">string</span>                                                                                      |
| `monthOfPlanting`         | The month of planting could be an interval (In case of a tree or shrub it's the month of flowering) | <span class="text-blue">number</span>, <span class="text-blue">number</span>[ ]                                            |
| `monthOfHarvest`          | The month of harvest could be an interval                                                           | <span class="text-blue">number</span>, <span class="text-blue">number</span>[ ]                                            |
| `plantingInterval`        | The number of years before replanting on the same area                                              | <span class="text-blue">number</span>, <span class="text-blue">null</span>                                                 |
| `plantation`              | A very short text to explain when and how to plant                                                  | <span class="text-blue">string</span>                                                                                      |
| `harvest`                 | A very short text to explain when and how to harvest                                                | <span class="text-blue">string</span>, <span class="text-blue">null</span>                                                 |
| `soil`                    | How should be the soil for a good grow                                                              | <span class="text-blue">string</span>, <span class="text-blue">null</span>                                                 |
| `tasks`                   | The work required between planting and harvesting                                                   | <span class="text-blue">string</span>, <span class="text-blue">null</span>                                                 |
| `favourableAssociation`   | The name of the favourable vegetables association                                                   | <span class="text-blue">string</span>[ ], <span class="text-blue">null</span>                                              |
| `unfavourableAssociation` | The name of the unfavourable vegetables association                                                 | <span class="text-blue">string</span>[ ], <span class="text-blue">null</span>                                              |
| `favourablePrecedents`    | Vegetables that can be on the growing area before planting                                          | <span class="text-blue">string</span>[ ], <span class="text-blue">null</span>                                              |
| `unfavourablePrecedents`  | Vegetables that can not be on the growing area before planting                                      | <span class="text-blue">string</span>[ ], <span class="text-blue">null</span>                                              |

    
### Example with a Tomato

```json
{
  "id": "toma",
  "name": "Tomato",
  "type": "fruit",
  "fromTree": false,
  "family": "Solanaceae",
  "synopsis": "Very simple to consume, it lends itself to an infinite number of preparations. Very rich in nutrition, it has real advantages for your well-being. All its qualities make it the most consumed vegetable in France.",
  "preservation": "3 to 4 days at room temperature",
  "nutritional": "20 kcal for 100g",
  "vitamin": "Rich in fiber and vitamin C",
  "history": "Discovered in South America in the 16th century, the tomato takes its name from an Aztec word: \"Tomatl\". After crossing the ocean, it reached the south of Europe. The Italians called it \"golden apple\" and the Provençals \"love apple\". Considered poisonous, its plants were cultivated for their ornamental qualities. The agronomist Olivier de Serres recommended it for the decoration of arbors. Its properties as a vegetable fruit were only discovered in the middle of the 18th century. It then gained all the tables of southern Europe. In France, the Provençals were the first to consume it. It was prepared in the form of sauces more or less raised.",
  "imagePath": "../../../assets/vegetables/icons/tomato.png",
  "cultureSheets": {
    "monthOfPlanting": 5,
    "monthOfHarvest": 7,
    "plantingInterval": 3,
    "plantation": "In the second part of May, as soon as there is no risk of frost. Choose well-built plants.",
    "harvest": "From July to September depending on the variety. To pick ripe tomatoes, grasp each one with your hands and pull it out, turning it like a pear. Cut off the stems of underripe tomatoes that you want to keep for a while. Before winter, wrap the last harvested fruits in newspaper, they will finish ripening.",
    "soil": "Lightly loose and rich in humus.",
    "tasks": "Keep a single stem and pinch off the shoots (false buds) that appear in the leaf axils. Tie the stem to a stake as the plant grows by securing it with raffia. Water regularly, preferably in the morning, avoiding wetting the foliage to avoid the development of mildew. Plant a row of marigolds nearby. Their yellow color and smell are repulsive. Keep the soil mulched and loose. Weed regularly. Be careful not to remove leaves in the belief that this will hasten fruit ripening, but at the end of the season you can remove yellowed or diseased leaves to allow the last of the fruit to ripen. To prevent fungal diseases, spray with Bordeaux mixture at planting,after the first pruning, and if the weather is too wet, a third time, avoiding the already formed fruits. As soon as the plants have recovered, you can also bring 1.5 liter of liquid manure for a 10 liter watering can and water the tomato plants twice at 15 days interval. Caution: Nettle manure is rich in nitrogen, do not overuse it at the risk of seeing the leaves develop to the detriment of the fruits.",
    "favourableAssociation": ["Celery", "Bean", "Onion", "Parsley", "Leek", "Basil", "Chives", "Nasturtium", "Lavender"],
    "unfavourableAssociation": null,
    "favourablePrecedents": null,
    "unfavourablePrecedents": null
  }
}
```
For other examples please visit this link : [MyGardenDB](https://m9712.gitlab.io/vegetables-db/MyGardenDB/tomato/)
