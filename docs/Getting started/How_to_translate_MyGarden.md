MyGarden uses Crowdin to translate the App.

All you need is to create a [Crowding](https://crowdin.com/) account and join the [projet](https://crowdin.com/project/mygardenapp).

## Translation process for Vegetables.

`Android -> Vegetable` mean : Repo `Android`, Branch `Vegetable`

`Android -> Translation` mean : Repo `Android`, Branch `Translation`

``` mermaid

graph TD
  A[data.json is generated on MyGardenDB] -->|CI/CD push| B[Android -> Vegetable];
  B --> C{Error?};
  C -->|yes| D[Debug];
  C -->|no| E[Adroid -> Translation]
  E -->|Fetch| F[Crowdin]
  D --> B;
```

## Translation structure
!!! warning
    
    You don't have to edit these files directly, please go through Crowdin for that.


- `src/assets/i18n`: Translation files of the App core
- `src/assets/vegetables/*.json`: Translation files of vegetables.
