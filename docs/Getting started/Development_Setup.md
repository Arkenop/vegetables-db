MyGarden is an app build with [Ionic](https://ionicframework.com/), [Angular](https://angular.io/) and [Capacitor](https://capacitorjs.com/).

## Download Required Tools

Download and install these right away to ensure an optimal Ionic development experience:

- **Node.js** for interacting with the Ionic ecosystem. Download the LTS version here.
- **A code editor** for... writing code! We are fans of Visual Studio Code.
- **Command-line interface/terminal (CLI)**:

    * **Windows** users: for the best Ionic experience, we recommend the built-in command line (cmd) or the Powershell CLI, running in Administrator mode.
    * **Mac/Linux** users, virtually any terminal will work.

## Install the Ionic CLI

Install the Ionic CLI with npm:

```shell
npm install -g @ionic/cli
```

## Get MyGarden files

Clone the [repository](https://gitlab.com/m9712/mygarden):
```shell
git clone https://gitlab.com/m9712/mygarden.git
```

```shell
cd mygarden/
```
## Run the App

Install the dependency:
```shell
npm install
```

Run this command next:
```shell
ionic serve
```
Your Ionic app is now running in a web browser. Most of your app can be built and tested right in the browser, greatly increasing development and testing speed.

## Build for Android (For development only)
First follow this [guide](https://ionicframework.com/docs/developing/android) to install and configure Android Studio.

Run these commands next:
```shell
ng build
```
```shell
ionic capacitor copy android
```
```shell
ionic capacitor run android
```

## Basic structure

- **MyGarden App**
  
    * `src/`: Common part and the MyGarden app code
    * `package.json`: npm specific parts
    * `src/assets`: some resources (mostly images but also translation files) which are used in the project. Most of the are embedded to the code.
    * `src/app/services`: Angular services
    * `src/app/shared`: Angular model
    * `src/app/tab1`: First tab with vegetables overview
    * `src/app/tab2`: Second tab with Garden editor
    * `src/app/tab3`: Third tab with information about the app, authors...


- **MyGardenDB**

    * `mkdocs.yml`: Doc build configuration
    * `docs`: Docs folder
    * `docs/MyGardenDB`: vegetable data-base
    * `docs/assets`: Vegetable icons
    * `docs/stylesheets`: Custom css
    * `.gitlab/template/new-vegetable.md`: Vegetable template o
