fileStorage="${1}" #Get the file path from the argument

rm -f "$fileStorage"
declare -a filesArray #Array of the files paths

touch $fileStorage

#Get the files paths
i=0
for f in docs/MyGardenDB/*.md;
do
  filesArray[$i]=$f
  echo $f
  i=$i+1
done;

#Extract JSONs except for the last one
length=${#filesArray[@]}
i=0
for j in "${filesArray[@]}"
do
  i=$(( $i + 1))
  if [[ "$i" == "$length" ]]; then break; fi
  awk "/\[jsonStart\]/,/\[jsonEnd\]/" "$j" |head -n -2 | tail -n +3 >> $fileStorage #Extract the JSON and add it into $fileStorage
  sed -i '$ d' $fileStorage # Remove } by removing the last line.
  echo '},' >> $fileStorage # Add }, at the end of the file.
done

awk "/\[jsonStart\]/,/\[jsonEnd\]/" "$j" |head -n -2 | tail -n +3 >> $fileStorage #Extract the last json and add it into $fileStorage
sed -i '1 s/^/[\n/' $fileStorage # Add '[' at the top.
echo ']' >> $fileStorage #Add ']' at the bottom
