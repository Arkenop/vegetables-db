#Check JSON file created from jsonBuilder

from argparse import ArgumentParser # parser with doc
import os
import json

__VERSION__ = '0.1'

def check_json(path: str, verbose: bool, all_checks: bool) -> int:
	res = 0
	typeAllowed = ['fruit', 'vegetable', 'plant']
	if not os.path.exists(path): return 1
	with open(path, 'r') as file:
		data = json.load(file)
	# check for duplicated ids
	if verbose: print('check for duplicated ids :')
	if not all([True if 'id' in item.keys() else False for item in data]):
		if verbose: print('items are invalid : no id field')
		return 1
	id_list = [item['id'] for item in data]
	if verbose: print(f'\tid list : {", ".join(id_list)}')
	for id in id_list:
		if id_list.count(id) > 1: # is a duplicate
			if verbose: print('\tid {} is duplicated ({} values)'.format(id, id_list.count(id)))
			res = 1
			if not all_checks:
				return res
	# check for type
	if verbose: print('check for type :')
	if not all([True if 'type' in item.keys() else False for item in data]):
		if verbose: print('items are invalid : no id field')
		return 1
	type_list = [(item['type'], item['id']) for item in data]
	if verbose: print(f'\ttype list : {", ".join([type for type,_ in type_list])}')
	for type, id in type_list:
		if type not in typeAllowed:
			if verbose: print('\ttype {} is invalid at id {}'.format(type, id))
			res = 1
			if not all_checks:
				return res
 	#check for name image 
	if verbose: print('check for name image :')
	if not all([True if 'type' in item.keys() else False for item in data]):
		if verbose: print('items are invalid : no id field')
		return 1
	name_png = [(item['imagePath'].split('/')) for item in data]
	b = len(name_png)
	for i in range(b):
		print(name_png[i][-1].replace(".png", ""))
	name_list = [(item['name'].lower()) for item in data]
	for name in name_list:
		if name not in name_list:
			res = 1
			if not all_checks:
				return res

	# other checks...

	return res



if __name__ == "__main__":
	parser = ArgumentParser(description="Checks the json at given path for saving in db")
	parser.add_argument("path", type=str, help="path of the json file to check")
	parser.add_argument('--all_checks', '-a', action='store_true', default=False,
					help="if set, the script will always do all checks even if one failed before")             
	parser.add_argument('--verbose', '-vb', action='store_true', default=False,
					help="if set, the script will print out all actions and used data for checks for debug")
	parser.add_argument('--version', '-v', action='version', version=f'check json script v{__VERSION__}, by Raphaël')
	kwargs = vars(parser.parse_args())

	exit(check_json(**kwargs))
