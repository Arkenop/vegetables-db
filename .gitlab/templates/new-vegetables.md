[//]: # (---)
[//]: # (tags:)
[//]: # (- ⚠ Need review ⚠)
[//]: # (---)

[//]: # (Thank you very much for your contribution to the open source world!)

[//]: # (If the JSON is not complete or if there are problems, uncomment the 4 lines at the top.)

[//]: # (To add a vegetable please follow the next steps)
[//]: # (Everything must be written in English, it will be translated later !)

[//]: # (1. Replace <vegetable-name> by the vegetables name in lowercase)
[//]: # (2. download the vegetable icon in the docs/assets/icons/ directory)
[//]: # (The image will not be visible during the development process)
<figure markdown>
  ![Image title](https://gitlab.com/m9712/vegetables-db/-/raw/main/docs/assets/icons/<vegetable-name>.png){ width="150" }
</figure>

[//]: # (3. Replace <Author name> and <Author-website>)
[//]: # (4. Replace <image-url> by the link where you found the image)
- [Author: <Author name>](<Author-website>)
- [image info](<image-url>)

### JSON

[//]: # (https://m9712.gitlab.io/vegetables-db/Getting%20started/add-vegetables/ for more information)
[//]: # (Please, replace <*> by the correct information)
[//]: # (The "id" is the first four letters of the name in lower case)
[jsonStart]:  # (Please don't remove this line)
```json
{
  "id": "<id>",
  "name": "<vegetable-name with the first letter in capital letter>",
  "type": "<"fruit" | "vegetable" | "plant">",
  "fromTree": <true | false>
  "family": "<vegetable family>",
  "synopsis": "<Short summary>",
  "preservation": "<7 to 8 days in ambient air>",
  "nutritional": "<kcal number> kcal for 100g",
  "vitamin": "Rich in vitamin <vitaminName>",
  "history": "<The vegetable history>",
  "imagePath": "../../../assets/vegetables/icons/<vegetable-name>.png",
  "cultureSheets": {
    "monthOfPlanting": <number or [number, number]>,
    "monthOfHarvest": <number or [number, number]>,
    "plantingInterval": <number>,
    "plantation": "<Some advice for the plantation (When, how...)>",
    "harvest": "<How and when to harvest>",
    "soil": "<How the soil should be>",
    "tasks": "<The different tasks during the growth.>",
    "favourableAssociation": ["<vegetable-name,...>"],
    "unfavourableAssociation": ["<vegetable-name,...>"],
    "favourablePrecedents": ["<vegetable-name,...>"],
    "unfavourablePrecedents": ["<vegetable-name,...>"]
  }
}
```
[jsonEnd]: # (Please don't remove this line)

### Other information

### Sources

[//]: # (Please list all sources used)
 - [<WebSite name>](<WebSite URL>)

### Contributors

[//]: # (Replace <contributor-name> by your name)
[//]: # (If you modify the file you can put your name at the top of the list)

* **<your name>** - <year> - <your email address>
